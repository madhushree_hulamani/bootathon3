![icon](http://vlabs.iitkgp.ernet.in/se/isad_static/isad/images/new/header_01.jpg)
![header](http://vlabs.iitkgp.ernet.in/se/isad_static/isad/images/new/header-blue.gif)

# <span style="color: #606060">_**Identifying the Requirements from Problem Statements**_</span>
<br>

## ![icon](http://vlabs.iitkgp.ernet.in/se/isad_static/isad/images/new/stock_task_32x32.png)<span style="color: brown">Objectives</span>
<span style="color: #606060">__After completing this experiment you will be able to:__</span>

 ![bullet](http://vlabs.iitkgp.ernet.in/se/isad_static/isad/images/new/icons/bullets/bullet_blue2_16x16.png)<span style="color: grey">Identify ambiguities, inconsistencies and incompleteness from a requirements specification

![bullet](http://vlabs.iitkgp.ernet.in/se/isad_static/isad/images/new/icons/bullets/bullet_blue2_16x16.png)<span style="color: grey">Identify and state functional requirements

![bullet](http://vlabs.iitkgp.ernet.in/se/isad_static/isad/images/new/icons/bullets/bullet_blue2_16x16.png)<span style="color: grey">Identify and state non-functional requirements

## ![icon](http://vlabs.iitkgp.ernet.in/se/isad_static/isad/images/new/hourglass_32x32.png)<span style="color: brown">Time Required</span>
><span style="color: grey">Around 3.00 hours </span>

## <span style="color: brown">Requirements</span>
 <span style="color: grey">Sommerville defines "requirement" [1] as a specification of what should be implemented. Requirements specify how the target system should behave. It specifies what to do, but not how to do. Requirements engineering refers to the process of understanding what a customer expects from the system to be developed, and to document them in a standard and easily readable and understandable format. This documentation will serve as reference for the subsequent design, implementation and verification of the system.</span>

<span style="color: grey">It is necessary and important that before we start planning, design and implementation of the software system for our client, we are clear about it's requirements. If we don't have a clear vision of what is to be developed and what all features are expected, there would be serious problems, and customer dissatisfaction as well. </span>

## <span style="color: brown">Characteristics of Requirements</span>
<span style="color: grey"> Requirements gathered for any new system to be developed should exhibit the following three properties:</span>

>![bullet](http://vlabs.iitkgp.ernet.in/se/isad_static/isad/images/new/icons/bullets/bullet_blue2_16x16.png)<span style="color: #606060">__Unambiguity__ : <span style="color: grey">There should not be any ambiguity what a system to be developed should do. For example, consider you are developing a web application for your client. The client requires that enough number of people should be able to access the application simultaneously. What's the "enough number of people"? That could mean 10 to you, but, perhaps, 100 to the client. There's an ambiguity.</span>

>![bullet](http://vlabs.iitkgp.ernet.in/se/isad_static/isad/images/new/icons/bullets/bullet_blue2_16x16.png)<span style="color: #606060">__Consistency__: <span style="color: grey">To illustrate this, consider the automation of a nuclear plant. Suppose one of the clients say that it the radiation level inside the plant exceeds R1, all reactors should be shut down. However, another person from the client side suggests that the threshold radiation level should be R2. Thus, there is an inconsistency between the two end users regarding what they consider as threshold level of radiation.</span>

>![bullet](http://vlabs.iitkgp.ernet.in/se/isad_static/isad/images/new/icons/bullets/bullet_blue2_16x16.png)<span style="color: #606060">__Completeness__: <span style="color: grey">A particular requirement for a system should specify what the system should do and also what it should not. For example, consider a software to be developed for ATM. If a customer enters an amount greater than the maximum permissible withdrawal amount, the ATM should display an error message, and it should not dispense any cash.</span>

## <span style="color: brown">Categorization of Requirements</span>
 <span style="color: grey">Based on the target audience or subject matter, requirements can be classified into different types, as stated below:</span>

   >![bullet](http://vlabs.iitkgp.ernet.in/se/isad_static/isad/images/new/icons/bullets/bullet_blue2_16x16.png)<span style="color: #606060"> __User requirements:__ <span style="color: grey">They are written in natural language so that both customers can verify their requirements have been correctly identified</span>

   >![bullet](http://vlabs.iitkgp.ernet.in/se/isad_static/isad/images/new/icons/bullets/bullet_blue2_16x16.png)<span style="color: #606060">__System requirements:__ <span style="color: grey">They are written involving technical terms and/or specifications, and are meant for the development or testing teams

Requirements can be classified into two groups based on what they describe:</span>

   >![bullet](http://vlabs.iitkgp.ernet.in/se/isad_static/isad/images/new/icons/bullets/bullet_blue2_16x16.png)<span style="color: #606060">__Functional requirements (FRs):__ <span style="color: grey">These describe the functionality of a system -- how a system should react to a particular set of inputs and what should be the corresponding output.</span>

   >![bullet](http://vlabs.iitkgp.ernet.in/se/isad_static/isad/images/new/icons/bullets/bullet_blue2_16x16.png)<span style="color: #606060">__Non-functional requirements (NFRs):__ <span style="color: grey">They are not directly related what functionalities are expected from the system. However, NFRs could typically define how the system should behave under certain situations. For example, a NFR could say that the system should work with 128MB RAM. Under such condition, a NFR could be more critical than a FR.

Non-functional requirements could be further classified into different types like:</span>

   >![bullet](http://vlabs.iitkgp.ernet.in/se/isad_static/isad/images/new/icons/bullets/bullet_blue2_16x16.png)<span style="color: #606060">__Product requirements:__ <span style="color: grey">For example, a specification that the web application should use only plain HTML, and no frames</span>

   >![bullet](http://vlabs.iitkgp.ernet.in/se/isad_static/isad/images/new/icons/bullets/bullet_blue2_16x16.png)<span style="color: #606060">__Performance requirements:__ <span style="color: grey">For example, the system should remain available 24x7
    Organizational requirements: The development process should comply to SEI CMM level 4</span>

## <span style="color: brown">Functional Requirements</span>
### __<span style="color: brown">Identifying Functional Requirements</span>__
 Given a problem statement, the functional requirements could be identified by focusing on the following points:

   ![bullet](http://vlabs.iitkgp.ernet.in/se/isad_static/isad/images/new/icons/bullets/bullet_blue2_16x16.png)Identify the high level functional requirements simply from the conceptual understanding of the problem. For example, a Library Management System, apart from anything else, should be able to issue and return books.

   ![bullet](http://vlabs.iitkgp.ernet.in/se/isad_static/isad/images/new/icons/bullets/bullet_blue2_16x16.png)Identify the cases where an end user gets some meaningful work done by using the system. For example, in a digital library a user might use the "Search Book" functionality to obtain information about the books of his interest.

   ![bullet](http://vlabs.iitkgp.ernet.in/se/isad_static/isad/images/new/icons/bullets/bullet_blue2_16x16.png)If we consider the system as a black box, there would be some inputs to it, and some output in return. This black box defines the functionalities of the system. For example, to search for a book, user gives title of the book as input and get the book details and location as the output.

   ![bullet](http://vlabs.iitkgp.ernet.in/se/isad_static/isad/images/new/icons/bullets/bullet_blue2_16x16.png)Any high level requirement identified could have different sub-requirements. For example, "Issue Book" module could behave differently for different class of users, or for a particular user who has issued the book thrice consecutively.

## <span style="color: brown">Preparing Software Requirements Specifications</span>
Once all possible FRs and non-FRs have been identified, which are complete, consistent, and non-ambiguous, the Software Requirements Specification (SRS) is to be prepared. IEEE provides a template [iv], also available here, which could be used for this purpose. The SRS is prepared by the service provider, and verified by its client. This document serves as a legal agreement between the client and the service provider. Once the concerned system has been developed and deployed, and a proposed feature was not found to be present in the system, the client can point this out from the SRS. Also, if after delivery, the client says a new feature is required, which was not mentioned in the SRS, the service provider can again point to the SRS. The scope of the current experiment, however, doesn't cover writing a SRS.  
<br>

[Made with Django](https://www.djangoproject.com/ "A Django powered site.")