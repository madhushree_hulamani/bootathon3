//select no. of forces
var s1: HTMLSelectElement = <HTMLSelectElement>document.getElementById("s1");
//Force table
var t1: HTMLTableElement = <HTMLTableElement>document.getElementById("t1");
//Adding no. of forces(rows) to select tag
for(let i = 2; i<=6; i++)
{
    var option: HTMLOptionElement = <HTMLOptionElement>document.createElement("option");
    option.text = i.toString();
    option.value = i.toString();
    s1.add(option);
}

//onchange selet tag create input matrix
function createinput()
{
    //deleteing force table
    deletetable(t1);

    //create input tab for force
    createtable(t1,"a");

}

//deletes all the rows
function deletetable(t1: HTMLTableElement)
{
    while(t1.rows.length > 0)
    {
        t1.deleteRow(0);
    }
}

//create table
function createtable(t1: HTMLTableElement, id: string)
{
    var row = +s1.value;
    var col = 2;
    for(let i=0; i<row; i++)
    {
        var t1row: HTMLTableRowElement = t1.insertRow();
        for(let j=0; j<col; j++)
        {
            var t1cell = t1row.insertCell();
            var text: HTMLInputElement = <HTMLInputElement>document.createElement("input");
            text.id = id + i + j;
            text.type = "text";
            t1cell.appendChild(text); 
        }
        
    } 

}

//button onclick RESULTANT
function resultant()
{
    var a: number[][] = [];
    //reading magnitude & angle
    readmat(a, +s1.value, 2, "a");
    //calculating resultant
    calculate(a);
}

//read the matrix
function readmat(a: number[][], row: number, col: number, id: string)
{
    //id->textbox id of matrix
    for(let i=0; i<row; i++)
    {
        a[i] = [];
        for(let j=0; j<col; j++)
        {
            let t: HTMLInputElement = <HTMLInputElement>document.getElementById(id + i + j);
            a[i][j] = +t.value;
        }
    }
}

//calculate the resultant of forces
function calculate(a: number[][])
{
    var sx: number = 0;
    var sy: number = 0;
    var ang: number;

    for(let i=0; i< +s1.value; i++)
    {
        sx += a[i][0] * Math.cos(Math.PI/180 * a[i][1]); //sumation of x;
        sy += a[i][0] * Math.sin(Math.PI/180 * a[i][1]); //sumation of y;
    }                       
    var result = Math.sqrt(Math.pow(sx,2) + Math.pow(sy,2)); //resultant magnitude
    ang = Math.atan2(sy,sx); //resultant angle
    
    console.log("Mag",result);
    console.log("Angle", ang * 180/ Math.PI);

}